from distutils.log import debug
from flask import Flask, render_template
import json


def getData(name):
    with open("./data/" + name + ".json", 'r+') as file:
        file_data = json.load(file)
        return file_data[name]


def getJson(filepath, name):
    with open(filepath, 'r+') as file:
        file_data = json.load(file)
        return file_data[name]

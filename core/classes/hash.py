import hashlib
import string
import random

# sha256 encryptipion
# TEST : 94ee059335e587e501cc4bf90613e0814f00a7b08bc7c648fd865a2af6a22cc2


class Hash:

    # convert param STRING to sha256 HASH
    def generate(self, strToHash="default"):
        hashed_string = hashlib.sha256(strToHash.encode('utf-8')).hexdigest()
        return hashed_string

    # convert param STRING to sha256 HASH
    def random(self, size=64, chars=string.ascii_uppercase + string.digits):
        return self.generate(''.join(random.choice(chars) for _ in range(size)))

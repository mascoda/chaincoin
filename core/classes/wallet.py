import time
from hashlib import sha256
# from .chain import Chain
# from .mempool import Mempool
# import json
from core.classes.hash import Hash
from core.func.appendData import appendData
from core.func.getData import getJson
import json


class Wallet:

    filepath = "./data/blockchain/wallets.json"

    def __init__(self):
        pass

    def create(self):

        hash = Hash()

        wallet = {
            "timestamp": time.time(),
            "id": hash.random(),
            "key": hash.random(),
            "value": 1.0000
        }
        appendData(self.filepath, "wallets", wallet)
        return wallet

    def get(self, wallet_id):
        wallets = getJson(self.filepath, "wallets")
        for wallet in wallets:
            json_acceptable_string = wallet.replace("'", "\"")
            w = json.loads(json_acceptable_string)
            if (w['id'] == wallet_id):
                return w
        return False

    def setValue(self, wallet_id, amount):
        wallets = getJson(self.filepath, "wallets")
        for wallet in wallets:
            json_acceptable_string = wallet.replace("'", "\"")
            w = json.loads(json_acceptable_string)
            if (w['id'] == wallet_id):
                w['value'] = amount
        return False
from datetime import datetime
import random
from .hash import Hash
# from .block import Block
from .chain import Chain
import json


class Mempool:

    mempool = "./data/mempool.json"   

    def __init__(self):
        pass

    def get(self):
        f = open(self.mempool)
        data = json.load(f)
        f.close()
        unconfirmedTransactions = []
        for transaction in data['transactions']:
            # WTF - crappy syntax?!?
            json_acceptable_string = transaction.replace("'", "\"")
            w = json.loads(json_acceptable_string)
            unconfirmedTransactions.append(w)
        return unconfirmedTransactions

    def clear(self):
        f = open(self.mempool, "w")
        f.write(json.dumps({ "transactions": [] }))
        f.close()
        

## IMPORTS
from .chain import Chain


class Contract():  

  ## check is transaction valid
  ## return True is valid
  ## return False is not valid
  def isValid(self, unconfirmed_transaction):
    
    blockchain = Chain()

    for block in blockchain.get():
      # print(block)
      for transaction in block['transactions']:
        # print(transaction)
        ## domain already has registred / has already an owner
        if(transaction['domain'] == unconfirmed_transaction['domain']):          
          ## then check is auth_code valid -> transfer the ownership
          if(transaction['auth_code'] != unconfirmed_transaction['auth_code']):
            return False
        
    return True
from datetime import datetime
import time
import random
from .hash import Hash
# from .block import Block
from .chain import Chain
from .wallet import Wallet
import json


class Transaction:

    mempool = "./data/blockchain/mempool.json"

    def __init__(self, transaction):

        # amount
        self.amount = float(transaction['amount'])

        # sender_wallet
        self.sender_wallet = transaction['sender_wallet']

        # receiver_wallet
        self.private_key = transaction['private_key']

        # receiver_wallet
        self.receiver_wallet = transaction['receiver_wallet']

        # unique transaction id
        self.tx = self.generateTransactionId()
        transaction['tx'] = self.tx

        # current timestamp
        self.timestamp = time.time()
        transaction['timestamp'] = self.timestamp

        # response = self.isValid()
        # if (response != False):
        #     pass
        #     # add transaction to the mempool
        #     # self.addToMempool(transaction)
        # else:
        #     return response

    def isValid(self):

        # init wallet object
        wallet = Wallet()

        # check is private_key valid
        sender_wallet = wallet.get(self.sender_wallet)

        if (sender_wallet['key'] != self.private_key):
            return "Invalid private key"

        # check is amount valid
        if (float(sender_wallet['value']) < self.amount):
            return "Credit is not enough"

        # check if receiver exists
        if (wallet.get(self.receiver_wallet) == False):
            return "Receiver Wallet is not valid / not exists"

        return True

    def get(self):
        with open(self.mempool, 'w') as file:
            file_data = json.load(file)
            return file_data["transactions"]

    def push(self):

        # print(trans)
        self.addToMempool(self.__dict__)
        return

    def addToMempool(self):
        json_object = json.dumps(self.__dict__)
        self.write(self.mempool, json_object)
        return

    def write(self, filename, entry):
        with open(filename, 'r+') as file:
            # First we load existing data into a dict.
            file_data = json.load(file)
            # Join new_data with file_data inside emp_details
            file_data["transactions"].append(entry)
            # Sets file's current position at offset.
            file.seek(0)
            # convert back to json.
            json.dump(file_data, file, indent=4)

    def generateTransactionId(self):

        dt = datetime.now()
        ts = datetime.timestamp(dt)
        randInt = random.randint(0*9, 9*9)

        self.tx = Hash().generate(str(self.sender_wallet) +
                                  str(self.receiver_wallet)+str(randInt)+str(ts))
        return self.tx

import time
from hashlib import sha256
from .chain import Chain
from .mempool import Mempool
import json


class Block:
    
    index = None
    blockchainDB = "./data/blockchain.json",
    mempool = "./data/mempool.json",
    
    default_status = "pending"   
 
    
    def __init__(self):                 
        self.timestamp = time.time()

        Blockchain = Chain()
        self.index = Blockchain.getIndex()+1
        self.previous_hash = Blockchain.getCurrentHash()        

        self.difficulty = 2
        self.nonce = 0       

        self.transactions = []

    
    def addTransaction(self, transaction):
        self.transactions.append(transaction)

    def verify(self):
        self.hash = self.calculateHash()
    
    def getTransactions(self):
        return self.transactions

    def getHash(self):
        return self.hash

    def getIndex(self):
        return self.index
          
    
    def push(self, transaction):

        entry = {
            "tx": transaction.tx,
            "debtor": transaction.debtor,
            "creditor": transaction.creditor,
            "value": transaction.value,
            "status": self.default_status
            }

        json_object = json.dumps(entry)
        self.write("./data/blockchain.json",json_object)
        return True
    
    def write(self,filename, entry):        
        with open(filename,'r+') as file:
            # First we load existing data into a dict.
            file_data = json.load(file)
            # Join new_data with file_data inside emp_details
            file_data["blocks"].append(entry)
            # Sets file's current position at offset.
            file.seek(0)
            # convert back to json.
            json.dump(file_data, file, indent = 4)


    def calculateHash(self):

        Blockchain = Chain()
        previous_block = Blockchain.getCurrentBlock() 

        conString = "{0} {1} {2} {3} {4}".format(
                self.index,
                self.transactions,
                self.timestamp,
                self.previous_hash,
                self.nonce
            )        

        # conString = str(previous_block['index']) + str(previous_block['timestamp']) + str(previous_block['hash']) + str(previous_block['previous_hash'])
        computed_hash = sha256(conString.encode("utf-8")).hexdigest()
        
        # index + timestamp + transactions + previous_hash als hash =  Hallo Welt
        # computed_hash = sha256("Hallo Welt".encode("utf-8")).hexdigest()
        
        # per Schleife solange versuchen bis gültiger Hash errechnet wurde
        while not computed_hash.startswith("0" * self.difficulty):          
            # Nonce um 1 erhöhen
            self.nonce += 1
            # neuen Hash generieren in Abhängigkeit der neuen Nonce
            string_to_hash = conString + str(self.nonce)
            computed_hash = sha256(string_to_hash.encode("utf-8")).hexdigest()       

        return computed_hash
        # aktuellen Hash ausgeben
        # print("Aktueller Hash:", computed_hash)
        # print("-"*20)
        # print("Nonce:", self.nonce)
        # print("Gültiger Hash:", computed_hash)



            
    
    
    
    
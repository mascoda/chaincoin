from datetime import datetime
import random
from .hash import Hash
import json


class Chain:

    walletFile = "./data/wallets.json"
    blockchainDB = "./data/blockchain.json"
    genesisBlockFile = "./data/genesisBlock.json"
    default_value = 0.0
    default_name = "Chaincoin"
    default_name_short = "cc"
    private_key_prefix = ""
    public_key_prefix = ""
    private_key = None
    public_key = None

    def __init__(self):
        self.createGenesisIfNotExists()

    def get(self):
        f = open(self.blockchainDB)
        data = json.load(f)
        f.close()
        blocks = []
        for block in data['blocks']:            
            blocks.append(block)
        return blocks


    def add(self, block):
        with open(self.blockchainDB, 'r+') as file:
            # First we load existing data into a dict.
            file_data = json.load(file)
            # Join new_data with file_data inside emp_details
            file_data["blocks"].append(block)
            # Sets file's current position at offset.
            file.seek(0)
            # convert back to json.
            json.dump(file_data, file, indent=4)

    def createWallet(self):

        dt = datetime.now()
        ts = datetime.timestamp(dt)

        self.private_key = self.generatePrivateKey(),

        wallet = {
            "private_key": self.generatePrivateKey(),
            "public_key": self.generatePublicKey(),
            "value": self.default_value,
            "created": ts
        }

        json_object = json.dumps(wallet)
        self.write(json_object, self.walletFile)

        return wallet

    def generatePrivateKey(self):
        randInt = random.randint(5*9, 9999999999)
        dt = datetime.now()
        ts = datetime.timestamp(dt)
        self.private_key = self.private_key_prefix + \
            Hash().generate(str(ts) + str(randInt))
        return self.private_key

    def generatePublicKey(self):
        randInt = random.randint(0*9, 9999999999)
        dt = datetime.now()
        ts = datetime.timestamp(dt)
        return self.public_key_prefix + Hash().generate(self.private_key + str(ts) + str(randInt))

    def write(self, new_wallet, filename):
        with open(filename, 'r+') as file:
            # First we load existing data into a dict.
            file_data = json.load(file)
            # Join new_data with file_data inside emp_details
            file_data["wallets"].append(new_wallet)
            # Sets file's current position at offset.
            file.seek(0)
            # convert back to json.
            json.dump(file_data, file, indent=4)


    

    def getWallets(self):
        f = open(self.walletFile)
        data = json.load(f)
        collectedWallets = []
        for wallet in data['wallets']:
            # WTF - crappy syntax?!?
            json_acceptable_string = wallet.replace("'", "\"")
            d = json.loads(json_acceptable_string)
            collectedWallets.append({'public_key': d['public_key'], 'value': self.formatValue(
                d['value']), 'created': d['created']})
        f.close()
        return collectedWallets

    def getWallet(self, public_key):
        f = open(self.walletFile)
        data = json.load(f)
        f.close()
        for wallet in data['wallets']:
            # WTF - crappy syntax?!?
            json_acceptable_string = wallet.replace("'", "\"")
            w = json.loads(json_acceptable_string)
            if (w['public_key'] == public_key):
                return w['value']
        return False

    def getWalletByPK(self, private_key):
        f = open(self.walletFile)
        data = json.load(f)
        f.close()
        for wallet in data['wallets']:
            # WTF - crappy syntax?!?
            json_acceptable_string = wallet.replace("'", "\"")
            w = json.loads(json_acceptable_string)
            if (w['private_key'] == private_key):
                return w
        return False

    def formatValue(self, value):
        return str(value) + self.default_name_short + " (" + self.default_name + ")"

    def createGenesisIfNotExists(self):
        f = open(self.blockchainDB)
        data = json.load(f)
        f.close()

        if(len(data['blocks']) == 0):
            f = open(self.genesisBlockFile)
            genesisBlock = json.load(f)
            f.close()
            self.addBlock(genesisBlock)

    # returned lastBlockIndex from Blockchain as Integer
    def getIndex(self):
        f = open(self.blockchainDB)
        data = json.load(f)
        f.close()

        return len(data['blocks'])-1

    def getBlockByIndex(self, index):
        f = open(self.blockchainDB)
        data = json.load(f)
        f.close()

        return data['blocks'][index]
    
    def getCurrentBlock(self):
        f = open(self.blockchainDB)
        data = json.load(f)
        f.close()

        return data['blocks'][len(data['blocks'])-1]
    
    def getCurrentHash(self):
        f = open(self.blockchainDB)
        data = json.load(f)
        f.close()

        return data['blocks'][len(data['blocks'])-1]['hash']
           
            
        
        

# DOCS
# FLASK: https://flask.palletsprojects.com/en/2.2.x/
# JINJA: https://jinja.palletsprojects.com/en/3.1.x/templates/#template-inheritance


import logging
from distutils.log import debug
from flask import Flask, render_template, request, session, redirect, url_for
from core.classes.transaction import Transaction
from core.func.getData import getData
from core.classes.wallet import Wallet


app = Flask(__name__)
# Set the secret key to some random bytes. Keep this really secret!
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

# get mainnavigation
navigation = getData("navigation")


#
# PAGES
#

@app.route("/")
def index():
    return render_template("pages/index.html", navigation=navigation, showParticles=True, wrapper="dark-wrapper")


@app.route("/ico")
def ico():
    return render_template("pages/ico.html", navigation=navigation, wrapper="dark-wrapper")


@app.route("/explorer")
def explorer():
    return render_template("pages/explorer.html", navigation=navigation, wrapper="dark-wrapper")


blog_entries = getData("blog")


@app.route("/blog")
def blog__list():
    # WTF - why dont log??
    print("Log")
    logging.warning('Watch out!')
    return render_template("pages/blog__list.html", navigation=navigation, wrapper="dark-wrapper", data=blog_entries)


entries = getData("roadmap")


@app.route("/roadmap")
def roadmap():
    return render_template("pages/roadmap.html", navigation=navigation, wrapper="roadmap-wrapper", data=entries)


@app.route("/contact")
def contact():
    return render_template("pages/contact.html", navigation=navigation, wrapper="dark-wrapper")


@app.route("/blog/post/<post_id>")
def blog__single(post_id):
    return render_template("pages/blog__single.html", navigation=navigation, wrapper="dark-wrapper", id="post_id")

#
# DEEPLINKS
#


@app.route("/coins/<coin>")
def coin(coin):
    return render_template("pages/coin.html", navigation=navigation, coin=coin, wrapper="dark-wrapper")


@app.route("/login", methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        # session['wallet_id'] = request.form['wallet_id']
        wallet = Wallet()
        wallet = wallet.get(request.form['wallet_id'])
        if (wallet):
            session['wallet'] = wallet
            return redirect(url_for('wallet'))
            # return render_template("pages/wallet.html", navigation=navigation, wrapper="dark-wrapper", wallet=wallet)
        else:
            error = "Invalid Wallet-ID"
    return render_template("pages/login.html", navigation=navigation, wrapper="dark-wrapper", error=error)


@app.route("/wallet", methods=['GET', 'POST'])
def wallet():
    error = None
    valid_transaction_request = False
    response = None
    if request.method == 'POST':
        amount = request.form['amount']
        receiver_wallet = request.form['receiver_wallet']
        private_key = request.form['private_key']

        # VALIDATION
        # check is private_key valid
        # check is amount valid
        # check if receiver exists
        new_transaction = {
            "amount": amount,
            "sender_wallet": session['wallet']['id'],
            "receiver_wallet": receiver_wallet,
            "private_key": private_key
        }
        transaction = Transaction(new_transaction)
        response = transaction.isValid()      
        if (response == True):
            valid_transaction_request = True
            transaction.addToMempool()
            # return render_template("pages/wallet.html", navigation=navigation, wrapper="dark-wrapper", wallet=wallet)
        else:
            error = response
    return render_template("pages/wallet.html", navigation=navigation, wrapper="dark-wrapper", valid_transaction_request=valid_transaction_request, error=error, response=response)


@app.route("/new_wallet", methods=['GET', 'POST'])
def new_wallet():
    # new_wallet = False
    # red = request.form.post('wallet')
    wallet = Wallet()
    created_wallet = wallet.create()
    session['logged-in'] = True
    session['wallet'] = created_wallet
    return render_template("pages/new_wallet.html", navigation=navigation, wrapper="dark-wrapper", wallet=created_wallet)


app.run(debug=True)
